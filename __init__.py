# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import collection
from . import configuration
from . import account


def register():
    Pool.register(
        configuration.Configuration,
        configuration.AccountProcedure,
        configuration.Procedure,
        configuration.Level,
        collection.Tracking,
        collection.Collection,
        collection.CreateCollectionStart,
        collection.Voucher,
        collection.TrackingReportStart,
        collection.PortfolioStatusStart,
        collection.BillCollectionStart,
        account.AccountReceivablePayableStart,
        module='collection', type_='model')
    Pool.register(
        collection.CreateCollection,
        collection.TrackingReportWizard,
        collection.PortfolioStatus,
        collection.BillCollection,
        account.AccountReceivablePayable,
        module='collection', type_='wizard')
    Pool.register(
        collection.TrackingReport,
        collection.PortfolioStatusReport,
        collection.BillCollectionReport,
        account.AccountReceivablePayableReport,
        module='collection', type_='report')
