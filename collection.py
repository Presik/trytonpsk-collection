# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import datetime, date, timedelta
from trytond.model import Model, ModelView, ModelSQL, fields
from trytond.pyson import If, Eval
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Wizard, StateView, Button, StateAction, StateReport
from trytond.report import Report
from decimal import Decimal
import traceback
import copy
from .exceptions import ProcedureError
from trytond.i18n import gettext

_STATES = {
    'readonly': Eval('state') == 'done',
    }
_DEPENDS = ['state']

_TYPES = [
    ('phone', 'Phone'),
    ('mobile', 'Mobile'),
    ('email', 'E-Mail'),
    ('website', 'Website'),
    ('skype', 'Skype'),
    ('whatsapp', 'Whatsapp'),
    ('msm', 'MSM'),
    ('other', 'Other'),
]


class Tracking(ModelSQL, ModelView):
    'Tracking'
    __name__ = 'collection.tracking'
    _rec_name = 'name'
    collection = fields.Many2One('collection.collection', 'Collection',
                                 required=True,)
    date = fields.Date('Date')
    user = fields.Many2One('res.user', 'User', readonly=True)
    contact_method = fields.Selection(_TYPES, 'Contact Method', required=True,)
    contact = fields.Function(fields.Char('Contact', depends=['_parent_collection.party', 'contact_method']), 'on_change_with_contact')
    debt_to_day = fields.Numeric('Debt to Day', readonly=True, states={
                                 'readonly': Eval('state') != 'active', }, depends=_DEPENDS)
    parcial_payment = fields.Function(fields.Boolean(
        'Parcial Payment', readonly=True), 'get_parcial_payment')
    compromise_payment_date = fields.Date('Compromise Payment Day', states={
                                          'readonly': Eval('state') != 'active', }, depends=_DEPENDS)
    compromise_payment_amount = fields.Numeric('Compromise Payment',)
    customer_comments = fields.Text(
        'Comments', states={'readonly': Eval('state') != 'active', }, depends=_DEPENDS)
    state = fields.Function(fields.Selection([
            ('done', 'Done'),
            ('active', 'Active'),
            ('inactive', "Inactive"),
            ], 'State', readonly=True), 'get_state', )
    voucher_line = fields.Many2One(
        'account.voucher.line', 'Voucher Line', readonly=True)
    collection_amount = fields.Function(fields.Numeric(
        'Collection Amount', readonly=True), 'get_collection_amount')
    collection_percent = fields.Function(fields.Numeric(
        'Collection Percent', digits=(2, 2), readonly=True), 'get_collection_percent')

    @classmethod
    def __setup__(cls):
        super(Tracking, cls).__setup__()
        cls._order = [
            ('date', 'DESC'),
        ]

    @staticmethod
    def default_user():
        return Transaction().user

    @staticmethod
    def default_contact_method():
        return 'phone'

    @staticmethod
    def default_state():
        return 'active'

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @classmethod
    def create(cls, vals):
        Collection = Pool().get('collection.collection')
        for v in vals:
            if v.get('collection'):
                collection = Collection(v['collection'])
                v['debt_to_day'] = collection.pending_payment
        super(Tracking, cls).create(vals)

    def get_collection_amount(self, name):
        if self.voucher_line and self.voucher_line.voucher.move:
            return self.voucher_line.amount
        else:
            return 0

    @fields.depends('contact_method', 'collection', '_parent_collection.party')
    def on_change_with_contact(self, name=None):
        contact_method = self.contact_method
        if contact_method and self.collection:
            if contact_method == 'msm':
                contact_method = 'mobile'

            return self.collection.party.get_mechanism(contact_method)

    def get_collection_percent(self, name):
        if self.debt_to_day and self.collection_amount:
            return abs(round((self.collection_amount / self.debt_to_day), 2))

    def get_state(self, name):
        pool = Pool()
        Configuration = pool.get('collection.configuration')
        Date = pool.get('ir.date')
        try:
            configuration = Configuration(1)
        except Exception as e:
            raise

        if configuration:
            _date = self.date + \
                timedelta(days=configuration.tracking_days_expired)
            if _date > Date.today():
                return 'active'
            # validate states please
            if self.collection_amount and self.collection_amount > 0:
                return 'done'
            else:
                return 'inactive'

    def get_parcial_payment(self, name):
        if self.compromise_payment_amount and self.debt_to_day:
            val = self.debt_to_day - self.compromise_payment_amount
            if val > 0:
                return True
            else:
                return False


class Collection(ModelSQL, ModelView):
    'Collection'
    __name__ = 'collection.collection'
    company = fields.Many2One('company.company', 'Company', required=True,
        help="Make the collection belong to the company.",
        select=True, domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
            Eval('context', {}).get('company', -1)),
            ],
        states=_STATES, depends=_DEPENDS)
    description = fields.Char('Description')
    # line = fields.Many2One('account.move.line', 'Move Line', required=True,
    #     help="The receivable line to dun for.",
    #     domain=[
    #         ('account.type.receivable', '=', 'true'),
    #         ('account.company', '=', Eval('company', -1)),
    #         ['OR',
    #         ('debit', '>', 0),
    #             ('credit', '<', 0),
    #         ],
    #         ],
    #     states=_STATES, depends=_DEPENDS + ['company'])
    procedure = fields.Many2One('collection.procedure', 'Procedure',
                                states=_STATES, depends=_DEPENDS)
    level = fields.Function(fields.Many2One(
        'collection.level', 'Level', readonly=True), 'get_level')
    # level = fields.Many2One('collection.level', 'Level',
    #     domain=[
    #         ('procedure', '=', Eval('procedure', -1)),
    #         ],
    #     states=_STATES, depends=_DEPENDS + ['procedure'])
    # blocked = fields.Boolean('Blocked',
    #     help="Check to block further levels of the procedure.")
    active = fields.Function(fields.Boolean('Active'), 'get_active',
                             searcher='search_active')

    state = fields.Selection([
            ('running', "Running"),
            ('done', "Done"),
            ('cancel', "Cancel"),
            ], 'State', readonly=True)
    party = fields.Many2One('party.party', 'Party')
    origin = fields.Function(fields.Reference('Origin',
            selection=[
                ('', ''),
                ('account.invoice', 'account.invoice'),
                ], translate=False), 'get_origin')
    amount = fields.Function(fields.Numeric('Amount Total',
        digits=(
            16, Eval('currency_digits', 2)),
        depends=['currency_digits']),
        'get_amount')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
        'get_line_field')
    maturity_date = fields.Function(fields.Date('Maturity Date'),
        'get_maturity_date', searcher='search_maturity_date')
    expired_days = fields.Function(fields.Numeric('Expired Days'),
        'get_expired_days')
    total_payment = fields.Function(fields.Numeric('Total  Payment'),
        'get_total_payment')
    pending_payment = fields.Function(fields.Numeric('Pending  Payment'),
        'get_pending_payment')
    payments = fields.Function(fields.One2Many('account.move.line', None,
        'Payments'), 'get_payments')
    tracking = fields.One2Many(
        'collection.tracking', 'collection', 'Tracking',)
    amount_second_currency = fields.Function(fields.Numeric(
            'Amount Second Currency',
            digits=(16, Eval('second_currency_digits', 2)),
            depends=['second_currency_digits']), 'get_amount_second_currency')
    second_currency = fields.Function(fields.Many2One('currency.currency',
                                                      'Second Currency'), 'get_second_currency')
    second_currency_digits = fields.Function(fields.Integer(
            'Second Currency Digits'), 'get_second_currency_digits')
    collection_percent = fields.Function(fields.Numeric(
        'Collection Percent', digits=(2, 2), readonly=True), 'get_collection_percent')
    move = fields.Many2One("account.move", "Move")
    lines_move = fields.Function(fields.One2Many("account.move.line", "move", "Lines"), 'get_lines',
      searcher='search_lines_move')

    @classmethod
    def __setup__(cls):
        super(Collection, cls).__setup__()
        cls._buttons.update({
                'process': {
                    'depends': ['state'],
                    },
                })
        # cls._order = [
        #     ('line', 'DESC'),
        # ]

    @classmethod
    def __register__(cls, module_name):
        super(Collection, cls).__register__(module_name)
        table = cls.__table_handler__(module_name)
        if table.column_exist('line'):
            cursor = Transaction().connection.cursor()
            query = """UPDATE collection_collection
            SET move = a.move,
            description = a.description
            FROM (SELECT id, move, description from account_move_line) as a
            WHERE a.id = collection_collection.line
            AND collection_collection.move is NULL"""
            cursor.execute(query)
            query2 = """alter table collection_collection alter column line drop not null"""
            cursor.execute(query2)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'running'

    def get_lines(self, name=None):
        lines = []
        lines_append = lines.append
        if self.move:
            for ml in self.move.lines:
                if ml.account.type.receivable and self.party == ml.party \
                    and (self.origin or self.description == ml.description):
                    lines_append(ml.id)
            return lines

    @classmethod
    @ModelView.button
    def process(cls, collections):
        running = []
        done = []
        for collection in collections:
            if collection.state == 'running' and collection.pending_payment <= 0:
                done.append(collection)
            elif collection.state == 'done' and collection.pending_payment > 0:
                running.append(collection)
        cls.write(running, {'state': 'running'})
        cls.write(done, {'state': 'done'})

    def get_active(self, name):
        return all([ml.reconciliation for ml in self.lines_move])

    def get_level(self, name):
        if self.procedure and self.procedure.levels and self.expired_days:
            level_ids = []
            level_ids.append(None)
            num = 0
            for l in self.procedure.levels:
                if num >= 1:
                    if l.collect_days <= self.expired_days:
                        if level_ids[0].collect_days <= l.collect_days:
                            level_ids[0] = l
                else:
                    level_ids[0] = l
                num += 1
            return level_ids[0].id

    def get_maturity_date(self, name):
        line = None
        for ml in self.lines_move:
            if not ml.reconciliation:
                line = ml
                break
            line = ml
        try:
            value = getattr(line, name)
        except Exception:
            print('error', self.id)
            value = None
        return value

    def get_line_field(self, name):
        line = None
        for ml in self.lines_move:
            if not ml.reconciliation:
                line = ml
                break
            line = ml
        try:
            value = getattr(line, name)
        except Exception:
            print('error', self.id)
            value = None
        return value

    def get_expired_days(self, name):
        if self.maturity_date and self.pending_payment != 0:
            Date = Pool().get('ir.date')
            return int((Date.today() - self.maturity_date).days)
        else:
            return None

    def get_total_payment(self, name):
        return int(sum([pay_line.amount for pay_line in self.payments]))

    def get_origin(self, name):
        if self.move and self.move.origin:
            return str(self.move.origin)

    def get_pending_payment(self, name):
        return int(self.amount - abs(self.total_payment or Decimal(0.0)))

    def get_payments(self, name=None):
        lines = []
        try:
            origin = self.move.origin
        except Exception:
            origin = None
        if origin:
            if origin.state == 'paid':
                lines = [ln.id for ln in origin.reconciliation_lines]
            else:
                lines = [ln.id for ln in origin.payment_lines]
        return lines

    def get_collection_percent(self, name):
        if self.amount and self.total_payment:
            return abs(round((self.total_payment / self.amount), 2))

    @classmethod
    def search_line_field(cls, name, clause):
        return [('line.' + clause[0],) + tuple(clause[1:])]

    def get_amount(self, name):
        if self.lines_move:
            return sum(line.debit - line.credit for line in self.lines_move)
        else:
            return 0

    def get_amount_second_currency(self, name):
        amount = self.line.debit - self.line.credit
        if self.line.amount_second_currency:
            return self.line.amount_second_currency.copy_sign(amount)
        else:
            return amount

    def get_second_currency(self, name):
        if self.line.second_currency:
            return self.line.second_currency.id
        else:
            return self.line.account.company.currency.id

    def get_second_currency_digits(self, name):
        if self.line.second_currency:
            return self.line.second_currency.digits
        else:
            return self.line.account.company.currency.digits

    @classmethod
    def search_active(cls, name, clause):
        reverse = {
            '=': '!=',
            '!=': '=',
            }
        if clause[1] in reverse:
            if clause[2]:
                return [('lines_move.reconciliation', clause[1], None)]
            else:
                return [('lines_move.reconciliation', reverse[clause[1]], None)]
        else:
            return []

    @classmethod
    def search_lines_move(cls, name, clause):
        # FIX ME
        # reverse = {
        #     '=': '!=',
        #     '!=': '=',
        #     }
        # if clause[1] in reverse:
        #     if clause[2]:
        #         return [('lines_move.reconciliation', clause[1], None)]
        #     else:
        #         return [('lines_move.reconciliation', reverse[clause[1]], None)]
        # else:
        return []

    @classmethod
    def _overdue_line_domain(cls, date):
        return [
            ('account.type.receivable', '=', 'true'),
            # ('collections', '=', None),
            ('maturity_date', '<=', date),
            ('maturity_date', '!=', None),
            ['OR',
                ('debit', '>', 0),
                ('credit', '<', 0),
             ],
            ('party', '!=', None),
            ('reconciliation', '=', None),
        ]

    @classmethod
    def generate_collections(cls, date=None):
        pool = Pool()
        Date = pool.get('ir.date')
        MoveLine = pool.get('account.move.line')

        if date is None:
            date = Date.today()

        lines = MoveLine.search(cls._overdue_line_domain(date))
        move_ids = list(set(ml.move.id for ml in lines))
        moves = list(set(ml.move for ml in lines))
        cls.search([], limit=1)
        moves_exist = cls.search([('move', 'in', move_ids)])
        moves_exist = [c.move.id for c in moves_exist]
        collections = []
        for move in moves:
            if move.id not in moves_exist:
                collections.extend(cls._get_collection(move, date))
        cls.save([d for d in collections if d])

    @classmethod
    def _get_collection(cls, move, date):
        if move.origin and move.origin.amount_to_pay == 0:
            return []
        elif not move.origin:
            c = {}
            for line in move.lines:
                if not line.account.type.receivable:
                    continue
                try:
                    key = str(line.party.id) + line.reference
                except Exception as e:
                    traceback.print_exc()
                    print(line.move.number, 'validate line id', line.id, str(e))
                if line.maturity_date and not line.reconciliation and key not in c.keys():
                    c[key] = cls(
                        move=move,
                        procedure=None,
                        level=None,
                        party=line.party,
                        description=line.reference
                    )
            if len(list(c.keys())):
                return c.values()

        return [cls(
            move=move,
            procedure=None,
            level=None,
            party=move.origin.party,
        )]


class CreateCollectionStart(ModelView):
    'Create Collection'
    __name__ = 'collection.create.start'
    date = fields.Date('Date', required=True,
                       help="Create Collection up to this date.")

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()


class CreateCollection(Wizard):
    'Create Collection'
    __name__ = 'collection.create'
    start = StateView('collection.create.start',
                      'collection.collection_create_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Create', 'create_',
                                 'tryton-ok', default=True),
                          ])
    create_ = StateAction('collection.act_collection_form')

    def do_create_(self, action):
        pool = Pool()
        collection = pool.get('collection.collection')
        collection.generate_collections(date=self.start.date)
        return action, {}

#
# class ProcessCollectionStart(ModelView):
#     'Create Collection'
#     __name__ = 'collection.process.start'
#
#
# class ProcessCollection(Wizard):
#     'Process Collection'
#     __name__ = 'collection.process'
#     start = StateView('collection.process.start',
#         'collection.collection_process_start_view_form', [
#             Button('Cancel', 'end', 'tryton-cancel'),
#             Button('Running', 'running', 'tryton-ok', default=True),
#         ])
#     process = StateTransition()
#
#     @classmethod
#     def __setup__(cls):
#         super(ProcessCollection, cls).__setup__()
#
#         # _actions is the list that define the order of each state to process
#         # after the 'process' state.
#         cls._actions = ['running']
#
#     def next_state(self, state):
#         "Return the next state for the current state"
#         try:
#             i = self._actions.index(state)
#             return self._actions[i + 1]
#         except (ValueError, IndexError):
#             return 'end'
#
#     def transition_process(self):
#         pool = Pool()
#         collection = pool.get('collection.collection')
#         collections = collection.browse(Transaction().context['active_ids'])
#         collection.process(collections)
#         return self.next_state('running')


class Voucher(metaclass=PoolMeta):
    __name__ = 'account.voucher'

    def set_state_collection(self, line):
        Tracking = Pool().get('collection.tracking')
        Collection = Pool().get('collection.collection')
        trackings = Tracking.search([
            ('voucher_line', '=', line.id),
        ])
        if trackings:
            Collection.write([trackings[0].collection], {'state': 'running'})

    @classmethod
    def cancel(cls, vouchers):
        super(Voucher, cls).cancel(vouchers)
        for voucher in vouchers:
            for line in voucher.lines:
                voucher.set_state_collection(line)

    @classmethod
    def draft(cls, vouchers):
        super(Voucher, cls).draft(vouchers)
        for voucher in vouchers:
            for line in voucher.lines:
                voucher.set_state_collection(line)

    @classmethod
    def post(cls, vouchers):
        super(Voucher, cls).post(vouchers)
        Tracking = Pool().get('collection.tracking')
        Collection = Pool().get('collection.collection')
        for voucher in vouchers:
            if voucher.voucher_type != 'receipt':
                continue
            for line in voucher.lines:
                if not line.move_line:
                    continue
                trackings = Tracking.search([
                    ('date', '<=', voucher.date),
                    ('collection.party.id', '=', voucher.party.id),
                    ('collection.move.id', '=', line.move_line.move),
                ])
                if trackings:
                    index = len(trackings) - 1
                    if not trackings[index].voucher_line:
                        Tracking.write([trackings[index]], {
                                       'voucher_line': line.id})
                    if line.move_line.move_origin.state == 'paid' or line.move_line.move_origin.amount_to_pay == 0:
                        Collection.write([trackings[index].collection], {
                                         'state': 'done'})


class TrackingReportStart(ModelView):
    'Tracking Report Start'
    __name__ = 'collection.tracking_report.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date("Start Date", required=True)
    end_date = fields.Date("End Date", required=True)
    party = fields.Many2One('party.party', "party")

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end_date():
        Date = Pool().get('ir.date')
        return Date.today()


class TrackingReportWizard(Wizard):
    'Tracking Report Wizard'
    __name__ = 'collection.tracking_wizard'
    start = StateView('collection.tracking_report.start',
                      'collection.tracking_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Print', 'print_', 'tryton-ok', default=True),
                          ])
    print_ = StateReport('collection.tracking_report')

    def do_print_(self, action):
        party_id = None
        if self.start.party:
            party_id = self.start.party.id
        data = {
            'ids': [],
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'party': party_id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class TrackingReport(Report):
    __name__ = 'collection.tracking_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Tracking = pool.get('collection.tracking')

        tracking_filtered = [
            ('collection.company', '=', data['company']),
            ('date', '>=', data['start_date']),
            ('date', '<=', data['end_date']),
        ]

        if data['party']:
            tracking_filtered.append(
                ('collection.party.id', '=', data['party']),
            )

        trackings = Tracking.search(tracking_filtered, order=[
                                    ('collection.party.name', 'ASC')])

        report_context['records'] = trackings
        report_context['company'] = Company(data['company'])
        report_context['start'] = data['start_date']
        report_context['end'] = data['end_date']

        return report_context


class PortfolioStatusStart(ModelView):
    'Portfolio Status Start'
    __name__ = 'collection.print_portfolio_status.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    detailed = fields.Boolean('Detailed')
    date_to = fields.Date('Date to')
    category_party = fields.MultiSelection('get_category_party', 'Party Categories')
    kind = fields.Selection([
        ('in', 'Supplier'),
        ('out', 'Customer'),
    ], 'Kind', required=True)
    payment_terms = fields.MultiSelection('get_payment_terms', 'Payment Term')

    procedures = fields.MultiSelection('get_procedures', "Procedures")
    # _get_types_cache = Cache('party.address.subdivision_type.get_types')

    @classmethod
    def get_procedures(cls):
        pool = Pool()
        Procedure = pool.get('collection.procedure')
        procedures = Procedure.search_read([], fields_names=['id', 'name'])
        return [(r['id'], r['name']) for r in procedures]

    @classmethod
    def get_category_party(cls):
        pool = Pool()
        PartyCategory = pool.get('party.category')
        categories = PartyCategory.search_read([('active', '=', True)], fields_names=['id', 'name'])
        return [(r['id'], r['name']) for r in categories]

    @classmethod
    def get_payment_terms(cls):
        pool = Pool()
        PaymentTerm = pool.get('account.invoice.payment_term')
        payment_terms = PaymentTerm.search_read([], fields_names=['id', 'name'])
        return [(r['id'], r['name']) for r in payment_terms]

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_group():
        return 'customers'


class PortfolioStatus(Wizard):
    'Portfolio Status'
    __name__ = 'collection.print_portfolio_status'

    start = StateView('collection.print_portfolio_status.start',
                      'collection.print_portfolio_status_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Print', 'print_', 'tryton-ok', default=True),
                          ])
    print_ = StateReport('collection.portfolio_status_report')

    def do_print_(self, action):
        data = {
            'ids': [],
            'company': self.start.company.id,
            'detailed': self.start.detailed,
            'kind': self.start.kind,
            'category_party': list(self.start.category_party),
            'procedures': list(self.start.procedures),
            'payment_terms': list(self.start.payment_terms),
            'date_to': self.start.date_to,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class PortfolioStatusReport(Report):
    'Portfolio Status Report'
    __name__ = 'collection.portfolio_status_report'

    @classmethod
    def get_domain_invoice(cls, data):
        domain = [
            ('company', '=', data['company']),
            ('type', '=', data['kind']),
        ]
        if data['category_party']:
            domain.append(('party.categories', 'in', data['category_party']))
        if data['payment_terms']:
            domain.append(('payment_term', 'in', data['payment_terms']))
        if data['date_to']:
            to_date = data['date_to']
            dt = datetime.combine(
                date(to_date.year, to_date.month, to_date.day), datetime.min.time())
            dom = [
                'OR',
                [
                    ('payment_lines.date', '>=', data['date_to']),
                    # ('write_date', '>=', dt),
                    ('state', '=', 'paid'),
                ],
                ('state', '=', 'posted')
            ]
            domain.append(dom)
            domain.append(('invoice_date', '<=', data['date_to']))
        else:
            domain.append(('state', '=', 'posted')),
        return domain

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Invoice = pool.get('account.invoice')
        Line = pool.get('account.move.line')
        company = Company(data['company'])

        column_add = ''
        group_by_add = ''
        join_add = ''
        if 'operation_center' in Line._fields:
            column_add = ", CONCAT(op.code, ' - ', op.name) as operation_center"
            group_by_add = ', op.code, op.name'
            join_add = 'LEFT JOIN company_operation_center AS op ON op.id = ml.operation_center'

        dom_invoices = cls.get_domain_invoice(data)
        if data['procedures']:
            accounts = []
            Procedure = pool.get('collection.procedure')
            procedures = Procedure.search_read([
                    ('id', 'in', data['procedures'])
                ], fields_names=['accounts'])
            for procedure in procedures:
                accounts.extend(list(procedure['accounts']))
            if not accounts:
                raise ProcedureError(gettext('collection.msg_missing_account_procedure'))
            dom_invoices.append(['account', 'in', accounts])
        order = [('party.name', 'DESC'), ('invoice_date', 'ASC')]
        invoices = Invoice.search(dom_invoices, order=order)
        records = {}
        today = date.today()
        deepcopy = copy.deepcopy
        expired_kind = {
            'range_0': [],
            'range_1_30': [],
            'range_31_60': [],
            'range_61_90': [],
            'range_91': [],
        }
        expired_sums = deepcopy(expired_kind)
        expired_sums['total'] = []
        move_ids = []
        append_move_ids = move_ids.append
        for invoice in invoices:
            append_move_ids(invoice.move.id)
            if data['detailed']:
                key_id = str(invoice.party.id) + '_' + str(invoice.id)
            else:
                key_id = str(invoice.party.id)

            if key_id not in records.keys():
                _expired_kind = deepcopy(expired_kind)
                categories = invoice.party.categories
                records[key_id] = {
                    'party': invoice.party,
                    'category': categories[0].name if categories else '',
                    'total': [],
                    'notes': '',
                    'invoice': [],
                    'expired_days': '',
                }
                records[key_id].update(_expired_kind)

            time_forward = 0
            if data['date_to']:
                maturity_date = None
                move_lines_paid = []
                pay_to_date = []
                pay_append = pay_to_date.append
                line_append = move_lines_paid.append

                for line in invoice.payment_lines:
                    if line.move.date <= data['date_to']:
                        pay_append(abs(line.debit - line.credit))
                        line_append(line.id)
                    else:
                        # if line.maturity_date and line.maturity_date < maturity_date or maturity_date is None: fix
                        if line.maturity_date and (maturity_date is None or line.maturity_date < maturity_date):
                            maturity_date = line.maturity_date

                for line in invoice.move.lines:
                    line_id = line.id
                    if not line.reconciliation:
                        continue
                    for recline in line.reconciliation.lines:
                        if recline.id == line_id or line_id in move_lines_paid:
                            continue
                        if recline.move.date <= data['date_to']:
                            pay_append(abs(recline.debit - recline.credit))

                amount_paid = sum(pay_to_date)
                if not maturity_date:
                    maturity_date = invoice.estimate_pay_date or invoice.invoice_date

                time_forward = (data['date_to'] - maturity_date).days
                amount = invoice.total_amount - amount_paid
            else:
                amount = invoice.amount_to_pay
                if invoice.estimate_pay_date:
                    time_forward = (today - invoice.estimate_pay_date).days

            if time_forward <= 0:
                expire_time = 'range_0'
            elif time_forward <= 30:
                expire_time = 'range_1_30'
            elif time_forward <= 60:
                expire_time = 'range_31_60'
            elif time_forward <= 90:
                expire_time = 'range_61_90'
            else:
                expire_time = 'range_91'

            notes = None
            if hasattr(invoice, 'agent') and invoice.agent:
                notes = invoice.agent.rec_name
            if notes and not records[key_id]['notes']:
                records[key_id]['notes'] = notes

            records[key_id][expire_time].append(amount)
            records[key_id]['invoice'].append(invoice)
            records[key_id]['expired_days'] = time_forward
            records[key_id]['total'].append(amount)
            expired_sums[expire_time].append(amount)
            expired_sums['total'].append(amount)

        move_lines_without_invoice = {}
        if data['detailed']:
            cond1 = 'where'
            if data['category_party']:
                cat_ids = str(tuple(data['category_party'])).replace(',', '') if len(
                    data['category_party']) == 1 else str(tuple(data['category_party']))
                cond1 = f'''where pcr.category in %s and''' % (
                    cat_ids)

            cond2 = ''
            if move_ids:
                cond2 = 'and ml.id not in %s' % (str(tuple(move_ids)).replace(
                    ',', '') if len(move_ids) == 1 else str(tuple(move_ids)))

            cond3 = ''
            if data['date_to']:
                cond3 = ' and am.date <= %s' % (
                    data['date_to'].strftime("'%Y-%m-%d'"))
            cond4 = ''
            if data['procedures']:
                cond4 = ' and ml.account in %s' % (str(tuple(accounts)).replace(
                    ',', '') if len(accounts) == 1 else str(tuple(accounts)))
            type_ = 'receivable'
            if data['kind'] == 'in':
                type_ = 'payable'

            cursor = Transaction().connection.cursor()
            query_collection = f"""SELECT ml.id, ml.move, pp.name, pc.name AS category {column_add}, pp.id_number, ml.description, ml.reference, am.date, am.number, ml.maturity_date, ac.code, (current_date-ml.maturity_date::date) AS expired_days, COALESCE(sum(av.amount), 0) AS payment_amount,
                CASE WHEN (current_date-ml.maturity_date::date)<=0
                THEN (ml.debit-ml.credit) - COALESCE(sum(av.amount), 0) ELSE 0
                END AS range_0,
                CASE WHEN (current_date-ml.maturity_date::date)<=30 AND (current_date-ml.maturity_date::date) > 0
                THEN (ml.debit-ml.credit) - COALESCE(sum(av.amount), 0) ELSE 0
                END AS range_1_30,
                CASE WHEN (current_date-ml.maturity_date::date)<=60 AND (current_date-ml.maturity_date::date) > 30
                THEN (ml.debit-ml.credit) - COALESCE(sum(av.amount), 0) ELSE 0
                END AS range_31_60,
                CASE WHEN (current_date-ml.maturity_date::date)<=90 AND (current_date-ml.maturity_date::date) > 60
                THEN (ml.debit-ml.credit) - COALESCE(sum(av.amount), 0) ELSE 0
                END AS range_61_90,
                CASE WHEN (current_date-ml.maturity_date::date)>90
                THEN (ml.debit-ml.credit) - COALESCE(sum(av.amount), 0) ELSE 0
                END AS range_91,
                ((ml.debit-ml.credit) - COALESCE(sum(av.amount), 0)) AS total
                from account_move_line AS ml
                LEFT JOIN account_account AS ac
                ON ml.account = ac.id LEFT JOIN account_account_type AS at
                ON ac.type = at.id LEFT JOIN account_voucher_line AS av
                ON ml.id=av.move_line LEFT JOIN account_move AS am
                ON am.id=ml.move LEFT JOIN party_party AS pp
                ON pp.id=ml.party LEFT JOIN (SELECT DISTINCT ON (party) party, category from party_category_rel) AS pcr
                ON pcr.party=pp.id LEFT JOIN party_category AS pc
                ON pcr.category=pc.id
                {join_add}
                %s at.{type_}='t' AND ac.reconcile='t' AND ml.maturity_date is not null AND am.origin is null AND ml.reconciliation is null
                %s %s %s
            group by ml.id, ml.move, pp.name, pc.name {group_by_add}, pp.id_number, ml.description, ml.reference, am.date, am.number, ml.maturity_date, ac.code, expired_days, ml.debit, ml.credit;""" % (cond1, cond2, cond3, cond4)
            cursor.execute(query_collection)
            columns = list(cursor.description)
            result = cursor.fetchall()

            for row in result:
                row_dict = {}
                for i, col in enumerate(columns):
                    try:
                        expired_sums[col.name].append(row[i])
                    except:
                        pass
                    row_dict[col.name] = row[i]
                move_lines_without_invoice[row[0]] = row_dict

        lines_without_inv = move_lines_without_invoice.values()
        report_context['lines_without_inv'] = lines_without_inv
        report_context.update(expired_sums)
        report_context['records'] = records.values()
        report_context['company'] = company.party.name
        return report_context


class BillCollectionStart(ModelView):
    'Bill Collection Start'
    __name__ = 'collection.print_bill_collection.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class BillCollection(Wizard):
    'Bill Collection'
    __name__ = 'collection.print_bill_collection'
    start = StateView('collection.print_bill_collection.start',
                      'collection.print_bill_collection_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Print', 'print_', 'tryton-ok', default=True),
                          ])
    print_ = StateReport('collection.bill_collection_report')

    def do_print_(self, action):
        data = {
            'ids': [],
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'company': self.start.company.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class BillCollectionReport(Report):
    'Bill Collection'
    __name__ = 'collection.bill_collection_report'

    @classmethod
    def get_move_line(cls, line):
        origin = line['move_line.']['move.']['origin.']
        Invoice = Pool().get('account.invoice')
        salesman = ''
        invoice_number = line['move_line.']['description']
        if origin:
            invoice, = Invoice.search([('number', '=', origin['number'])])
            salesman = invoice.salesman.party.name if invoice.salesman else ''
            invoice_number = origin['number']
        value = {
            'number': line['voucher.']['number'],
            'invoice': invoice_number,
            'salesman': salesman,
            'date': line['voucher.']['date'],
            'total_amount': origin['total_amount'] if origin else abs(line['move_line.']['credit'] - line['move_line.']['debit']),
            'party': line['voucher.']['party.']['name'],
            'id_number': line['voucher.']['party.']['id_number'],
            'maturity_date': line['move_line.']['maturity_date'],
            'days_past_due': (line['voucher.']['date'] - line['move_line.']['maturity_date']).days,
            'amount': abs(line['amount']),
            'create_uid_name': line['voucher.']['create_uid.']['name'],
        }
        return value

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        VoucherLine = pool.get('account.voucher.line')
        company = Company(data['company'])

        vo_lines = VoucherLine.search_read([
            ('voucher.company', '=', data['company']),
            ('voucher.voucher_type', '=', 'receipt'),
            ('voucher.date', '>=', data['start_date']),
            ('voucher.date', '<=', data['end_date']),
            ('voucher.state', 'in', ['posted']),
            ('move_line', '!=', None),
            ('amount', '>', 0),
            ], fields_names=['amount', 'voucher.number', 'voucher.date',
                             'voucher.party.name', 'voucher.party.id_number',
                             'voucher.create_uid.name', 'move_line.maturity_date', 'move_line.debit',
                             'move_line.credit', 'move_line.move.origin.total_amount',
                             'move_line.move.origin.number', 'move_line.description']
        )
        move_lines = []
        sum_total = []
        add_sum_total = sum_total.append
        move_lines_append = move_lines.append
        for ln in vo_lines:
            value = cls.get_move_line(ln)
            try:
                rate = value['amount'] / value['total_amount']
                value['rate'] = rate
            except:
                pass
            move_lines_append(value)
            add_sum_total(value['amount'])

        report_context['records'] = move_lines
        report_context['sum_total'] = sum(sum_total)
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        report_context['company'] = company.party.name
        return report_context
