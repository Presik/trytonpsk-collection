# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields, sequence_ordered
from trytond.pyson import Eval


class Configuration(ModelSQL, ModelView):
    'Collection Configuration'
    __name__ = 'collection.configuration'
    tracking_days_expired = fields.Integer('Days to Expired of Tracking',
        required=True)
    company = fields.Many2One('company.company', 'Company', required=True)


class AccountProcedure(ModelSQL):
    'Account Procedure'
    __name__ = 'account.account.procedure'
    account = fields.Many2One('account.account', 'Account',
        ondelete='CASCADE', select=True, required=True)
    procedure = fields.Many2One('collection.procedure', 'Procedure', ondelete='CASCADE', select=True, required=True)


class Procedure(ModelSQL, ModelView):
    'Collection Procedure'
    __name__ = 'collection.procedure'
    name = fields.Char('Name', required=True, translate=True,
                       help="The main identifier of the Collection Procedure.")
    levels = fields.One2Many('collection.level', 'procedure', 'Levels')

    accounts = fields.Many2Many('account.account.procedure', 'procedure',
        'account', 'Account', domain=[
            ('closed', '!=', True),
            ('type', '!=', None),
            ('company', '=', Eval('context', {}).get('company', -1)),
            ])


class Level(sequence_ordered(), ModelSQL, ModelView):
    'Collection Level'
    __name__ = 'collection.level'
    procedure = fields.Many2One('collection.procedure', 'Procedure',
                                required=True, select=True)
    name = fields.Char('Name')
    collect_days = fields.Numeric('Days Collect')

    def get_rec_name(self, name):
        return self.name

    def test(self, line, date):
        if self.collect_days is not None:
            return int((date - line.maturity_date).days) >= self.collect_days
