# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from dateutil.relativedelta import relativedelta
from trytond.model import ModelView, fields
# from trytond.pyson import If, Eval
from sql import Column, Null, Window, Literal
from sql.aggregate import Sum, Max, Min
from sql.conditionals import Coalesce, Case
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.report import Report
from decimal import Decimal

PRIMOS = [71, 67, 59, 53, 47, 43, 41, 37, 29, 23, 19, 17, 13, 7, 3]
# from trytond.i18n import gettext


class AccountReceivablePayableStart(ModelView):
    'Account Receivable Payable Start'
    __name__ = 'collection.account_receivable_payable.start'

    date = fields.Date('Date To')
    terms = fields.Char('Terms', help='write terms separed by comma \b example 0,30,60,90')
    type = fields.Selection([
            ('customer', 'Customers'),
            ('supplier', 'Suppliers'),
            ('customer_supplier', 'Customers and Suppliers'),
            ],
        "Type", required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    posted = fields.Boolean('Posted Move', help="Only include posted moves.")
    unit = fields.Selection([
            ('day', 'Days'),
            ('week', "Weeks"),
            ('month', 'Months'),
            ('year', "Years"),
            ], "Unit", required=True, sort=False)

    @staticmethod
    def default_terms():
        return '0,30,60,90'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def default_type(cls):
        return 'customer'

    @classmethod
    def default_unit(cls):
        return 'day'

    @classmethod
    def default_posted(cls):
        return False

    @classmethod
    def default_date(cls):
        return Pool().get('ir.date').today()


class AccountReceivablePayable(Wizard):
    'Account Receivable Payable'
    __name__ = 'collection.account_receivable_payable'
    start = StateView('collection.account_receivable_payable.start',
        'collection.account_receivable_payable_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateReport('collection.account_receivable_payable.report')

    def do_print_(self, action):
        try:
            terms = self.start.terms.split(',')
        except:
            terms = [0, 30, 60, 90]

        data = {
            'terms': terms,
            'date': self.start.date,
            'unit': self.start.unit,
            'type': self.start.type,
            'company': self.start.company.id,
            'posted': self.start.posted,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class AccountReceivablePayableReport(Report):
    __name__ = 'collection.account_receivable_payable.report'

    @classmethod
    def get_table_query(cls, data):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Move = pool.get('account.move')
        Reconciliation = pool.get('account.move.reconciliation')
        Account = pool.get('account.account')
        Type = pool.get('account.account.type')
        Party = pool.get('party.party')

        line = MoveLine.__table__()
        move = Move.__table__()
        reconciliation = Reconciliation.__table__()
        account = Account.__table__()
        type_ = Type.__table__()
        debit_type = Type.__table__()
        party = Party.__table__()

        company_id = data.get('company')
        date = data.get('date')
        with Transaction().set_context(date=None):
            line_query, _ = MoveLine.query_get(line)
        _type = data.get('type')
        kind = cls.get_kind(type_, _type)
        debit_kind = cls.get_kind(debit_type, _type)
        columns = [
            account.code.as_('Cuenta'),
            party.type_document.as_('Tipo Documento'),
            party.id_number.as_('Documento'),
            party.name.as_('Nombre'),
            party.type_person.as_('Tipo Persona'),
            party.ciiu_code.as_('Codigo CIIU'),
            move.company.as_('Compañia'),
            Sum((line.debit - line.credit)).as_('balance'),
            ]

        terms = cls.get_terms(data.get('terms'))
        factor = cls.get_unit_factor()
        # Ensure None are before 0 to get the next index pointing to the next
        # value and not a None value
        term_values = sorted(
            list(terms.values()), key=lambda x: ((x is not None), x or 0))

        line_date = Coalesce(line.maturity_date, move.date)
        for name, value in terms.items():
            if value is None or factor is None or date is None:
                columns.append(Literal(None).as_(name))
                continue
            cond = line_date >= (date - value * factor)
            idx = term_values.index(value)
            if idx + 1 < len(terms) and idx != 0:
                cond &= line_date < (
                    date - (term_values[idx - 1] or 0) * factor)
            elif idx + 1 == len(terms):
                cond = line_date < (
                    date - (term_values[idx-1] or 0) * factor)
            if _type == 'customer':
                columns.append(
                    Sum(Case((cond, line.debit), else_=0)).as_(name))
            else:
                columns.append(
                    Sum(Case((cond, line.credit), else_=0)).as_(name))

        cond = line_date <= date
        if _type == 'customer':
            columns.append(
                Sum(Case((cond, line.credit), else_=0)).as_('amount'))
        else:
            columns.append(
                Sum(Case((cond, line.debit), else_=0)).as_('amount'))

        return line.join(move, condition=line.move == move.id
            ).join(party, condition=line.party == party.id
            ).join(account, condition=line.account == account.id
            ).join(type_, condition=account.type == type_.id
            ).join(debit_type, 'LEFT',
                condition=account.debit_type == debit_type.id
            ).join(reconciliation, 'LEFT',
                condition=reconciliation.id == line.reconciliation
            ).select(*columns,
                where=(line.party != Null)
                & (kind | debit_kind)
                & line_query
                & ((line.reconciliation == Null)
                    | (reconciliation.date > date))
                & (move.date <= date)
                & (account.company == company_id),
                group_by=(move.company, party.id_number,
                    party.name,
                    party.ciiu_code,
                    account.code,
                    party.type_document,
                    party.type_person),
                having=((Sum(line.debit) - Sum(line.credit)) != 0))

    @classmethod
    def get_terms(cls, terms):
        terms_dict = {}
        for term in terms:
            terms_dict['term ' + term] = int(term)
        term_final = 'term >' + terms[len(terms) - 1]
        terms_dict.update({term_final:  int(terms[len(terms)-1])+1})
        return terms_dict

    @classmethod
    def get_unit_factor(cls):
        context = Transaction().context
        return {
            'year': relativedelta(years=1),
            'month': relativedelta(months=1),
            'week': relativedelta(weeks=1),
            'day': relativedelta(days=1)
            }.get(context.get('unit', 'day'))

    @classmethod
    def get_kind(cls, account_type, type_):
        if type_ == 'supplier':
            return account_type.payable
        elif type_ == 'customer':
            return account_type.receivable
        else:
            return Literal(False)

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        records = []
        if data['type'] == 'customer_supplier':
            for t in ('customer', 'supplier'):
                data['type'] = t
                query = cls.get_table_query(data)
                records_ = cls.get_values(query, records)
                records.extend(records_)
        else:
            query = cls.get_table_query(data)
            records = cls.get_values(query, records)
        report_context['records'] = records
        report_context['header_report'] = records[0].keys() if records else []
        return report_context

    @classmethod
    def get_values(cls, query, records):

        def get_check_digit(value, type_doc):
            if type_doc != '31':
                return None
            value = value.replace(".", "")
            if not value.isdigit():
                return None
            c = 0
            p = len(PRIMOS) - 1
            for n in reversed(value):
                c += int(n) * PRIMOS[p]
                p -= 1

            dv = c % 11
            if dv > 1:
                dv = 11 - dv
            return dv

        cursor = Transaction().connection.cursor()
        cursor.execute(*query)
        columns = list(cursor.description)[:-2]
        result = cursor.fetchall()
        records_append = records.append
        for row in result:
            row_dict = {}
            amount = row[-1]
            type_doc = row[1]

            for i, col in enumerate(columns):
                row_dict[col.name] = row[i]
                if i == 2:
                    row_dict['DV'] = get_check_digit(row[i], type_doc)
            if amount:
                terms_ = []
                for k in row_dict.keys():
                    if k.startswith('term') and row_dict[k] > 0:
                        terms_.append(k)
                if terms_:
                    for k in terms_[::-1]:
                        value = row_dict[k]
                        if value < amount:
                            amount -= row_dict[k]
                            row_dict[k] = 0
                        elif value >= amount:
                            row_dict[k] = value - amount
                            break
            records_append(row_dict)
        return records
